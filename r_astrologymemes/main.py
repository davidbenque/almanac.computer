# %%

import praw
import os
import requests
import json
# %%%

print('Starting ASTROMEME')

reddit = praw.Reddit(
    client_id=os.environ['reddit_id'],
    client_secret=os.environ['reddit_secret'],
    password=os.environ['reddit_password'],
    user_agent='almanac',
    username='air_pump')

print('logged in as: ', reddit.user.me())

# %%

astromemes = reddit.subreddit('astrologymemes')

save_path = os.environ['TMA_home'] + '/_website/_current_issue/'
image_file = save_path + 'astromeme.jpg'
data_file = save_path + 'astromeme_data.json'

# %%

for post in astromemes.top('day', limit=10):
    print(post.title, ' - ', post.url)
    post_url = post.url
    r = requests.get(post_url)
    file_name = post_url.split('/')[-1]
    extension = file_name.split('.')[-1]
    if extension == 'jpg':
        with open(image_file, 'wb') as f:
            f.write(r.content)
            f.close()
        post_data = {
            'title': post.title,
            'link': 'https://reddit.com' + post.permalink,
            'author': post.author.name,
            'score': post.score
        }
        with open(data_file, 'w') as f:
            json.dump(post_data, f)
            f.close()
        break
    else:
        print('Not a jpeg')

print('--- END ASTROMEME ---')
