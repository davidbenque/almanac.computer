import numpy as np
import pandas as pd
from random import shuffle
import os
# %%
print("Starting Lottomatic")

data = os.environ['TMA_home'] + "/_data/all_draws.csv"

df = pd.read_csv(data)
df.head()

# %%
features = df.loc[:, 'N1':'BN'].values
target = df['Wins'].values

# %%
from sklearn.naive_bayes import GaussianNB

clf = GaussianNB()
clf.fit(features, target)

# %%
def winning_nums():
    while True:
        numbers = list(range(1,60))
        shuffle(numbers)
        draw = []
        for n in range(7):
            draw.append(numbers.pop())
        draw = np.array(draw)
        draw = draw.reshape(1, -1)
        pred = clf.predict(draw)
        if pred == 0:
            continue
        else:
            print("Found winning numbers: ", draw)
            return(draw[0].tolist())
            break
# %%
nums = []

for n in range(5):
    nums.append(winning_nums())

# %%
output = os.environ['TMA_home'] + '/_website/_current_issue/lotto_numbers.js'

with open(output, 'w') as file:
    file.write('var lotto_preds = {};'.format(nums))
    file.close()
