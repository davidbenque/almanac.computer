'''
import _in addition to_ EA_functions.py
functions for personalised readings
'''
from EA_functions import *

def get_house(chart, house):
    for h in chart.houses:
        if h.num() == house:
            return h

def object_in_house(row, pos, user_chart, object, house):
    '''
    check if an object is in house x for the user
    '''
    chart = make_chart(row['date_string'], pos)
    obj = get_object(chart, object)
    h = get_house(user_chart, house)

    if h.hasObject(obj):
        return 1
    else:
        return 0

def new_moon_house(row, pos, user_chart, moon_in = None):
    '''
        A New Moon, with the Sun and the Moon conjunct,
        classically signals a new beginning.
    '''
    chart = make_chart(row['date_string'], pos)
    day_after = row.name + timedelta(days = 1)
    date_after = day_after.strftime('%Y/%m/%d')
    next_chart = make_chart(date_after, pos)

    moon = get_object(chart, 'Moon')
    moon_phase = chart.getMoonPhase()
    next_moon_phase = next_chart.getMoonPhase()

    house = get_house(user_chart, moon_in)

    moon_score = 0

    if moon_phase != "First Quarter" and next_moon_phase == "First Quarter":
        moon_score += 1
        if moon_in:
            if house.hasObject(moon):
                moon_score += 1
    return moon_score
