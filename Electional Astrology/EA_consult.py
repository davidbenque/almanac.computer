import pandas as pd
import numpy as np

from datetime import date, timedelta, datetime

from flatlib.geopos import GeoPos
from flatlib.datetime import Datetime
from flatlib.chart import Chart
from flatlib import const, angle, props
from flatlib.aspects import hasAspect, getAspect, aspectType

from EA_functions import *
from EA_consult_topics import *
from EA_consult_functions import *

from geopy.geocoders import Nominatim


# %%
# get users details as input

# defaults
user_name = "Jane Doe"
input_DOB = "1979/04/27"
input_TOB = "12:00"

user_name = input("User name: ")
input_DOB = input("DOB [YYYY/MM/DD]: ")
input_TOB = input("TOB [HH:MM]: ")
input_POB = input("POB: ")

try:
    geolocator = Nominatim(user_agent="my-app")
    location = geolocator.geocode(input_POB)
    print("location found: ", location.latitude, location.longitude)
except:
    print("Location not found, please try again")

user_DOB = Datetime(input_DOB, input_TOB)
user_POB = GeoPos(location.latitude, location.longitude)
user_chart = Chart(user_DOB, user_POB, IDs=const.LIST_OBJECTS)
user_sun = user_chart.getObject(const.SUN)
user_sign = user_sun.sign

print("-------------------")
print("The user is: ", user_sign)
print("-------------------")

user = {
    "Name": user_name,
    "DOB": input_DOB + " " + input_TOB,
    "POB": input_POB + " (" + str(location.latitude) + "," + str(location.longitude) + ")",
    "Sign": user_sign
}

# %%
# chart of the reading
now = date.today()
now_date = now.strftime('%Y/%m/%d')

timestamp = datetime.now()
timestamp = timestamp.strftime("%Y%m%d%H%M%S")

reading_date = Datetime(now_date)
reading_pos = GeoPos(51.49674, -0.17252)
reading_chart = Chart(reading_date, reading_pos, IDs=const.LIST_OBJECTS)

# set up calendar df
duration = 365
df = calendar(duration)


# %%
# Query functions

Q = input("Choose Query\nWhen to:\nGet [m]arried\nGo on a [f]irst date\n[s]chedule a meeting\n[o]pen a business\n[p]urchase a computer:\n")

if Q == "m":
    query_title = "get married"
    get_married(df, reading_pos, user_sign)
elif Q == "f":
    query_title = "go on a first date"
    first_date(df, reading_pos, user_chart)
elif Q == "s":
    query_title = "schedule a meeting"
    schedule_a_meeting(df, reading_pos)
elif Q == "o":
    query_title = "open a buisness"
    open_a_buisness(df, reading_pos, user_chart)
elif Q == "p":
    query_title = "purchase a computer"
    purchase_a_computer(df, reading_pos, user_chart)

print("\nWhen to {Q}".format(Q=query_title))

# %% Scoring
# Flatenning all criteria into one score for GO and one for NOGO

df.drop(['date_string'], axis=1, inplace=True)

from sklearn.preprocessing import MinMaxScaler

scaler = MinMaxScaler()
df_scaled = pd.DataFrame(scaler.fit_transform(df), columns=df.columns, index=df.index)
df_scaled.head()

go_cols = df_scaled.filter(like=" GO", axis = 1)
nogo_cols = df_scaled.filter(like="NOGO", axis = 1)
df_scaled['total_GO'] = go_cols.sum(axis=1)
df_scaled['total_NOGO'] = nogo_cols.sum(axis=1)
scores_df  = df_scaled[['total_GO', 'total_NOGO']]

scores_df = pd.DataFrame(scaler.fit_transform(scores_df), columns=scores_df.columns, index=scores_df.index)
scores_df.head()

print("Scoring completed")

# %% Best and worst Dates
single_score = scores_df.copy()
single_score['score'] = single_score['total_GO'] - single_score['total_NOGO']
single_score.drop(['total_GO', 'total_NOGO'], axis=1, inplace = True)
single_score_cal = single_score.copy() # save a chronological copy for plotting

single_score = single_score.sort_values(by='score', ascending=False)

num_days = 5
best_days = single_score.head(num_days)
worst_days = single_score.tail(num_days).sort_values(by='score')
print("-----------")
print("Best Days\n", best_days)
print("-----------")
print("Worst Days\n", worst_days)

# %% Prepare scores for calendar Plotting

from sklearn.preprocessing import MinMaxScaler
ss_cal = pd.DataFrame(scaler.fit_transform(single_score_cal), index=single_score_cal.index, columns=single_score_cal.columns)

import datetime as dt

def iso_date(row, i):
    '''return ISO [month, week, weekday][i] for a given datetime index'''
    date = row['date']
    iso = date.isocalendar()
    cols = [date.year, date.month, date.strftime('%b'),date.week, iso[2], date.day]
    return cols[i]

ss_cal["date"] = ss_cal.index

for idx, n in enumerate(['year', 'month', 'month_name', 'week', 'weekday', 'day']):
    ss_cal[n] = ss_cal.apply(lambda row: iso_date(row, idx), axis = 1)

ss_cal['month_id'] = ss_cal.apply(lambda row: str(row['year']) + '-' + str(row['month']), axis = 1)
ss_cal['week_id'] = ss_cal.apply(lambda row: str(row['year']) + '-' + str(row['week']), axis = 1)

# %% Build matrix for each month

month_frames = [] # list to store all months as dataframes

months = ss_cal.month_id.unique()

for m in months:
    month_matrix = pd.DataFrame(columns=range(1,8))
    month_days = pd.DataFrame(columns=range(1,8))

    m_df = ss_cal.loc[ss_cal['month_id'] == m]

    weeks = m_df.week_id.unique()

    for w in weeks:
        df = pd.DataFrame(columns=range(1,8))
        week_row = m_df.loc[ss_cal['week_id'] == w]
        week_num = w.split('-')[1]

        week_row_score = week_row.pivot(columns='weekday', values='score')
        week_row_score = pd.concat([df, week_row_score])
        month_matrix.loc[week_num] = week_row_score.sum(axis=0, min_count=1)

        week_row_dates = week_row.pivot(columns='weekday', values='day')
        week_row_dates = pd.concat([df, week_row_dates])
        month_days.loc[week_num] = week_row_dates.sum(axis=0)

    month_name = m_df.iloc[0].month_name + ". " + str(m_df.iloc[0].year)
    month_frames.append({'name':month_name, 'scores':month_matrix, 'days':month_days})


# %% Color map
from matplotlib.colors import LinearSegmentedColormap

# black and white colormap, for color see EA_main.py line 181
cmap_bw = LinearSegmentedColormap.from_list('mycmap', ['#000000', '#666666', '#FFFFFF'])

# %% Plotting
import matplotlib
matplotlib.use('Agg')

from matplotlib import pyplot as plt
import math

num_plots = len(month_frames)
plot_rows = 4
plot_cols = math.ceil(num_plots / plot_rows)

fig, axes = plt.subplots(plot_rows, plot_cols, figsize=(15,20), sharey=True)
axes = axes.ravel() #flatten subplots as 1 dim list

week_days = ['M', 'T', 'W', 'R', 'F', 'S', 'S']
axes[0].set(yticks=np.arange(7), yticklabels=week_days)

title_font = {'fontname':'Operator Mono', 'fontsize':30}
date_font = {'fontname':'Operator Mono', 'fontsize':25}

for idx, month in enumerate(month_frames):

    data = month['scores'].T
    axes[idx].imshow(data, interpolation='nearest', cmap=cmap_bw, aspect='auto')
    axes[idx].set_title(month['name'], **title_font)
    for tick in axes[idx].get_yticklabels():
        tick.set_fontname("Operator Mono")
        tick.set_fontsize(25)

    # Loop over data dimensions and annotate matrix with dates.
    days = month['days'].T.values
    d, w = data.shape
    for i in range(d):
        for j in range(w):
            if days[i,j] != 0.0:
                # TODO: spot best and worst days here and change date for a symbol
                text = axes[idx].text(j, i, int(days[i,j]),
                           ha="center", va="center", color="k", **date_font)

# Clear borders and x axes (also gets rid of empty plots)
for subplot in axes:
        subplot.set(xticks=[])
        subplot.yaxis.set_tick_params(width=0)
        subplot.spines['top'].set_visible(False)
        subplot.spines['right'].set_visible(False)
        subplot.spines['bottom'].set_visible(False)
        subplot.spines['left'].set_visible(False)

plt.subplots_adjust(hspace=0.9)
plt.tight_layout()

chart_name = '{ts}_cal.svg'.format(ts=timestamp)
plt.savefig('readings/' + chart_name)
# plt.show()

print("📅   Exported Calendar")

# %%
import dominate
from dominate.tags import *

title = "{ts} Electional Astrology for {n}"
doc = dominate.document(title=title.format(n=user_name, ts=timestamp))

with doc.head:
    link(rel='stylesheet', href='style.css')

## Header
header = doc.body.add(div(cls="header"))

logo = header.add(div(cls="logo"))
logo += "The Monistic Almanac"

title = header.add(div(cls="title"))
title += "When to " + query_title

user_deets = header.add(div(cls="user"))
reading_deets = header.add(div(cls="reading"))

for key, value in user.items():
    user_deets.add(key + ": " + value)
    user_deets.add(br())

reading_deets += "Reading on: {d}".format(d=now_date)
reading_deets += br()
reading_deets += "at Victoria & Albert Museum"
reading_deets += br()
reading_deets += "(51.49674,-0.17252)"

# Calendar and Tables
chart = doc.body.add(div(cls="chart_container"))
calendar = chart.add(div(cls="calendar"))
calendar += img(src=chart_name)

tables = chart.add(div(cls="tables"))

table_titles = ["Best Days", "Worst Days"]
for i, df in enumerate([best_days, worst_days]):
    tables += table_titles[i]
    tb = tables.add(table())
    tb += tr(td("Date"), td("Score"))
    for row in df.iterrows():
        date = row[0].strftime("%d %b %Y")
        score = '%.5f'%(row[1].values[0])
        tb += tr(td(date), td(score))

# footer
footer = doc.body.add(footer())
footer.add("https://ALMANAC.COMPUTER")


file_name = "{ts}-{n}.html"
with open('readings/' + file_name.format(ts=timestamp, n=user_name), 'w') as f:
    f.write(doc.render())
print("📄  Exported Report")
