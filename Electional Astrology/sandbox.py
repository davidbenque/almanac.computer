# %% Imports
import pandas as pd
from flatlib.geopos import GeoPos
from EA_functions import *
from EA_topics import *

%matplotlib inline

# %% Set up

pos = GeoPos(51.424264, -0.072600) #London
duration = 365
df = calendar(duration)


# %% When to purchase a computer

'''
Make sure that Mercury, the planet of communication, isn’t retrograde. Okay, I know I keep mentioning this influence. It’s always important, but there are times — I admit it — when you can bend the rules. Not in this case, though. Do not — I repeat, do not — purchase a computer (or a car) when Mercury is spinning backward.
'''

df['mercury_direct GO'] = df.apply(lambda row: must_be_direct(row, pos, 'Mercury', GO='GO'), axis = 1)
df['mercury_direct NOGO'] = df.apply(lambda row: must_be_direct(row, pos, 'Mercury', GO='NOGO'), axis = 1)

# %%

'''
Make sure that Uranus and Mars aren’t doing anything unfortunate. High-tension squares, oppositions, and conjunctions, especially to Mercury or the Moon, are just the sort of thing you don’t want to see.
'''

df['UM_angle NOGO'] = df.apply(lambda row: auspicious_angle(row, pos, 'Uranus', 'Mercury', GO='NOGO'), axis = 1)
df['UMoo_angle NOGO'] = df.apply(lambda row: auspicious_angle(row, pos, 'Uranus', 'Moon', GO='NOGO'), axis = 1)

df['MM_angle NOGO'] = df.apply(lambda row: auspicious_angle(row, pos, 'Mars', 'Mercury', GO='NOGO'), axis = 1)
df['MMoo_angle NOGO'] = df.apply(lambda row: auspicious_angle(row, pos, 'Mars', 'Moon', GO='NOGO'), axis = 1)

# %%
'''
Check that the Moon isn’t void-of-course.
'''

df['🌝_void NOGO'] = df.apply(lambda row: void_of_course(row, pos), axis = 1)

# %%
'''
Tip: It isn’t necessary, but an Aquarian influence — perhaps in the form of the Sun or Moon in that sign — makes sure that your technology is cutting edge.
'''

df['Sun_in_♒️ GO'] = df.apply(lambda row: sun_in(row, pos, 'Aquarius'), axis = 1)
df['Moon_in_♒️ GO'] = df.apply(lambda row: moon_in(row, pos, 'Aquarius'), axis = 1)

# %%
df['mercury_direct GO'].isnull().sum()

# %%

df.plot.area(subplots=True, figsize=(10,6))
#
# waxing_moon_df = df[['waxing_moon_♉️ GO', 'waxing_moon_♍️ GO', 'waxing_moon_♑️ GO']]
# waxing_moon_df.plot.area(subplots=True, figsize=(10,6), color="#00FF00"
