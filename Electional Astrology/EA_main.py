

# %% Imports
import pandas as pd
import numpy as np

from flatlib.geopos import GeoPos

from EA_functions import *
from EA_topics import *
# %%

import os

save_path = os.environ['TMA_home'] + '/_website/_current_issue/'

# %% Set up

city_name, city_coords = random_city()


pos = GeoPos(*city_coords)
duration = 365
df = calendar(duration)

questions = ["💒 get married 💒", "🖋 schedule a meeting 💼", "📈 open a buisness 💰", "💻 purchase a computer 🖥"]

Q = np.random.choice(questions)

if Q == "💒 get married 💒":
    get_married(df, pos)
elif Q == "🖋 schedule a meeting 💼":
    schedule_a_meeting(df, pos)
elif Q == "📈 open a buisness 💰":
    open_a_buisness(df, pos)
elif Q == "💻 purchase a computer 🖥":
    purchase_a_computer(df, pos)

f = open(save_path + 'EA_title.js', 'wb')
title = 'var title = "When to  '+ Q + ' in ' + city_name + '";'

f.write(title.encode('utf8'))
f.close()

print("EA Starting: {Q} in {C}".format(Q=Q, C=city_name))

# %% Scoring
# Flatenning all criteria into one score for GO and one for NOGO

df.drop(['date_string'], axis=1, inplace=True)

from sklearn.preprocessing import MinMaxScaler

scaler = MinMaxScaler()
df_scaled = pd.DataFrame(scaler.fit_transform(df), columns=df.columns, index=df.index)
df_scaled.head()

go_cols = df_scaled.filter(like=" GO", axis = 1)
nogo_cols = df_scaled.filter(like="NOGO", axis = 1)
df_scaled['total_GO'] = go_cols.sum(axis=1)
df_scaled['total_NOGO'] = nogo_cols.sum(axis=1)
scores_df  = df_scaled[['total_GO', 'total_NOGO']]

scores_df = pd.DataFrame(scaler.fit_transform(scores_df), columns=scores_df.columns, index=scores_df.index)
scores_df.head()

print("Scoring completed")

# %% Best and worst Dates
single_score = scores_df.copy()
single_score['score'] = single_score['total_GO'] - single_score['total_NOGO']
single_score.drop(['total_GO', 'total_NOGO'], axis=1, inplace = True)
single_score_cal = single_score.copy() # save a chronological copy for plotting

single_score = single_score.sort_values(by='score', ascending=False)

num_days = 5
best_days = single_score.head(num_days)
worst_days = single_score.tail(num_days).sort_values(by='score')

print("Best Days: ", best_days)
print("Worst Days: ", worst_days)

# position on plot
markers_pos = np.empty(num_days)
markers_pos.fill(1.0)

#%%
days = {'best': best_days, 'worst': worst_days}

for k, v in days.items():
    v.reset_index(level=0, inplace=True)
    v.to_csv(save_path + 'EA_days_' + k + '.csv', index = False, header = ['date', 'score'])

# %% Curve Plotting
import matplotlib
matplotlib.use('Agg')

from matplotlib import pyplot as plt

# For curve plotting only
# transp = 0.5
# plt.figure(figsize=(10,6))

# def plot(col, colour):
#     #plt.plot(scores_df.index, scores_df[col], c=colour, lw=0.2)
#     plt.fill_between(scores_df.index , scores_df[col], color=colour)


# plot('total_NOGO', (1,0,0,transp))
# plot('total_GO', (0,1,0,transp))

# marker_size = 250
# plt.scatter(best_days.index, markers_pos, marker_size, color='#00FF00', marker='^')
# plt.scatter(worst_days.index, markers_pos, marker_size, color='#FF0000', marker='X')

# plt.show()


# %% Prepare scores for calendar Plotting

from sklearn.preprocessing import MinMaxScaler
ss_cal = pd.DataFrame(scaler.fit_transform(single_score_cal), index=single_score_cal.index, columns=single_score_cal.columns)

# scale

import datetime as dt

def iso_date(row, i):
    '''return ISO [month, week, weekday][i] for a given datetime index'''
    date = row['date']
    iso = date.isocalendar()
    cols = [date.year, date.month, date.strftime('%b'),date.week, iso[2], date.day]
    return cols[i]

ss_cal["date"] = ss_cal.index

for idx, n in enumerate(['year', 'month', 'month_name', 'week', 'weekday', 'day']):
    ss_cal[n] = ss_cal.apply(lambda row: iso_date(row, idx), axis = 1)

ss_cal['month_id'] = ss_cal.apply(lambda row: str(row['year']) + '-' + str(row['month']), axis = 1)
ss_cal['week_id'] = ss_cal.apply(lambda row: str(row['year']) + '-' + str(row['week']), axis = 1)

# %% Build matrix for each month

month_frames = [] # list to store all months as dataframes

months = ss_cal.month_id.unique()

for m in months:
    month_matrix = pd.DataFrame(columns=range(1,8))
    month_days = pd.DataFrame(columns=range(1,8))

    m_df = ss_cal.loc[ss_cal['month_id'] == m]

    weeks = m_df.week_id.unique()

    for w in weeks:
        df = pd.DataFrame(columns=range(1,8))
        week_row = m_df.loc[ss_cal['week_id'] == w]
        week_num = w.split('-')[1]

        week_row_score = week_row.pivot(columns='weekday', values='score')
        week_row_score = pd.concat([df, week_row_score])
        month_matrix.loc[week_num] = week_row_score.sum(axis=0, min_count=1)

        week_row_dates = week_row.pivot(columns='weekday', values='day')
        week_row_dates = pd.concat([df, week_row_dates])
        month_days.loc[week_num] = week_row_dates.sum(axis=0)

    month_name = m_df.iloc[0].month_name + ". " + str(m_df.iloc[0].year)
    month_frames.append({'name':month_name, 'scores':month_matrix, 'days':month_days})


# %% Color map
from matplotlib.colors import LinearSegmentedColormap

cmap = LinearSegmentedColormap.from_list('mycmap', ['#FF0000', '#EEEEEE', '#00FF00'])

# %% Plotting
import math

num_plots = len(month_frames)
plot_rows = 4
plot_cols = math.ceil(num_plots / plot_rows)

fig, axes = plt.subplots(plot_rows, plot_cols, figsize=(15,20), sharey=True)
axes = axes.ravel() #flatten subplots as 1 dim list

week_days = ['M', 'T', 'W', 'R', 'F', 'S', 'S']
axes[0].set(yticks=np.arange(7), yticklabels=week_days)

title_font = {'fontname':'Inconsolata', 'fontsize':30}
date_font = {'fontname':'Inconsolata', 'fontsize':27}

for idx, month in enumerate(month_frames):

    data = month['scores'].T
    axes[idx].imshow(data, interpolation='nearest', cmap=cmap, aspect='auto')
    axes[idx].set_title(month['name'], **title_font)
    for tick in axes[idx].get_yticklabels():
        tick.set_fontname("Inconsolata")
        tick.set_fontsize(25)

    # Loop over data dimensions and annotate matrix with dates.
    days = month['days'].T.values
    d, w = data.shape
    for i in range(d):
        for j in range(w):
            if days[i,j] != 0.0:
                # TODO: spot best and worst days here and change date for a symbol
                text = axes[idx].text(j, i, int(days[i,j]),
                           ha="center", va="center", color="k", **date_font)

# Clear borders and x axes (also gets rid of empty plots)
for subplot in axes:
        subplot.set(xticks=[])
        subplot.yaxis.set_tick_params(width=0)
        subplot.spines['top'].set_visible(False)
        subplot.spines['right'].set_visible(False)
        subplot.spines['bottom'].set_visible(False)
        subplot.spines['left'].set_visible(False)

plt.subplots_adjust(hspace=0.9)
plt.tight_layout()

plt.savefig(save_path + 'EA_calendar.svg')
# plt.show()

print("📅 Exported Calendar")
print("# END EA #")
