
from EA_functions import *

def get_married(df, pos):
    # Venus must be direct
    df['venus_direct GO'] = df.apply(lambda row: must_be_direct(row, pos, 'Venus', GO='GO'), axis = 1)
    df['venus_direct NOGO'] = df.apply(lambda row: must_be_direct(row, pos, 'Venus', GO='NOGO'), axis = 1)

    # Mercury must be direct
    df['mercury_direct GO'] = df.apply(lambda row: must_be_direct(row, pos, 'Mercury', GO='GO'), axis = 1)
    df['mercury_direct NOGO'] = df.apply(lambda row: must_be_direct(row, pos, 'Mercury', GO='NOGO'), axis = 1)

    # New moon is better
    df['new_moon GO'] = df.apply(lambda row: new_moon(row, pos), axis = 1)
    # Failing that a waxing moon at least
    df['waxing_moon GO'] = df.apply(lambda row: waxing_moon(row,pos), axis = 1)

    # Sun and Moon at an auspicious angle
    df['sun_moon_angle GO'] = df.apply(lambda row: auspicious_angle(row, pos, 'Moon', 'Sun'), axis = 1)
    df['sun_moon_angle NOGO'] = df.apply(lambda row: auspicious_angle(row, pos, 'Moon', 'Sun', GO='NOGO'), axis = 1)

    # Count trines and sextiles between Moon, Venus and Jupiter
    df['MVJ_trines_sextiles GO'] = df.apply(lambda row: count_trines_sextiles(row, pos, ['Moon', 'Venus', 'Jupiter']), axis = 1)

    # Check that the Moon isn't void of course
    df['moon_VOC NOGO'] = df.apply(lambda row: void_of_course(row, pos), axis = 1)


def schedule_a_meeting(df, pos):
    '''
    If you hope the meeting will help you inaugurate a program, introduce a new
    set of goals, involve a staff member who hasn’t previously participated in
    this area, or make a case for organizational change, schedule the meeting
    when the Moon is New or, at minimum, waxing.
    '''

    df["new_moon GO"] = df.apply(lambda row: new_moon(row, pos), axis = 1)
    df["waxing_moon GO"] = df.apply(lambda row: waxing_moon(row, pos), axis = 1)

    '''
    To encourage brainstorming, look for a conjunction, sextile, or trine between
    Mercury and Uranus, the planet of originality.
    The Moon in Gemini also encourages an explosion of ideas.
    '''

    df['MU_trines_sextiles GO'] = df.apply(lambda row: count_trines_sextiles(row, pos, ['Mercury', 'Uranus']), axis = 1)


    '''The Moon in Gemini also encourages an explosion of ideas.'''

    df['moon_in_♊️ GO'] = df.apply(lambda row: moon_in(row, pos, 'Gemini'), axis = 1)

    '''
    If you want the meeting to reach a final decision about an issue that’s been
    on the table many times before, schedule it for a time when the Moon is close
    to full. Full Moons can be times of high emotion. They’re very illuminating
    — they reveal that which has been hidden and they bring things to a head.
    If that’s what you have in mind, go ahead.
    '''

    df['before_full_🌝 GO'] = df.apply(lambda row: full_moon_buildup(row, pos), axis = 1)

    '''
     Make sure the Moon isn’t void-of-course and that Mercury isn’t retrograde.
    '''

    df['🌝_void NOGO'] = df.apply(lambda row: void_of_course(row, pos), axis = 1)
    df['mercury_retrograde NOGO'] = df.apply(lambda row: must_be_direct(row, pos, 'Mercury', GO='NOGO'), axis = 1)
    df['🌘_waning NOGO'] = df.apply(lambda row: waning_moon(row, pos), axis = 1)

def open_a_buisness(df, pos):
    '''
    Launching a business isn’t so different from starting a marriage, and some of the same rules apply. To wit:
    Make sure that Mercury, the planet that rules contracts, isn’t retrograde. The same goes for Venus, the planet of money.
    '''

    df['mercury_direct GO'] = df.apply(lambda row: must_be_direct(row, pos, 'Mercury', GO='GO'), axis = 1)
    df['mercury_direct NOGO'] = df.apply(lambda row: must_be_direct(row, pos, 'Mercury', GO='NOGO'), axis = 1)
    df['venus_direct GO'] = df.apply(lambda row: must_be_direct(row, pos, 'Venus', GO='GO'), axis = 1)
    df['venus_direct NOGO'] = df.apply(lambda row: must_be_direct(row, pos, 'Venus', GO='NOGO'), axis = 1)


    '''
    Start your enterprise on or shortly after a New Moon. A New Moon in your second house (or in Taurus) is ideal if the business is primarily financial or if it involves material objects. (A New Moon in the second house is also the right time to ask for a raise.) A New Moon in your sixth house (or in Virgo) is perfect if your business is service-oriented. And a New Moon in the tenth house (or in Capricorn) supports public awareness of your busi- ness and guarantees that you’ll be recognized in your field.
    '''

    df['waxing_moon_♉️ GO'] = df.apply(lambda row: waxing_moon(row, pos, moon_in="Taurus"), axis = 1)
    df['waxing_moon_♍️ GO'] = df.apply(lambda row: waxing_moon(row, pos, moon_in="Virgo"), axis = 1)
    df['waxing_moon_♑️ GO'] = df.apply(lambda row: waxing_moon(row, pos, moon_in="Capricorn"), axis = 1)


    '''
    Look for beneficial aspects (sextiles and trines) between Saturn and Jupiter. Saturn rules structures and organizations; Jupiter rules luck and expansion. You want them working together. Avoid squares and oppositions involving those two planets.
    '''

    df['SJ_trines_sextiles GO'] = df.apply(lambda row: count_trines_sextiles(row, pos, ['Mars', 'Venus']), axis = 1)
    df['SJ_angle NOGO'] = df.apply(lambda row: auspicious_angle(row, pos, 'Saturn', 'Jupiter', GO='NOGO'), axis = 1)


    '''
    An astrological calendar can tell you when an aspect is exact or at its peak. But an aspect between two planets often creates a buzz even before the crucial moment, when the aspect is approaching. It’s like Christmas: You can feel it in the air well in advance of the actual day. Afterwards, the energy diminishes quickly. I suggest that you scan ahead in your calendar to see if any major aspects are approaching. Be aware that if you open your business on a Wednesday, and Thursday there’s an opposition between Saturn and Jupiter, you’ll feel the tension.
    '''

    # To Do

    # from flatlib import const, angle, props
    # from flatlib.ephem import swe
    #
    #
    # def angle_test(row, pos, planet1, planet2):
    #     chart = chart = make_chart(row['date_string'], pos)
    #     #sun = swe.sweObjectLon(const.SUN, jd)
    #     p1 = get_object(chart, planet1)
    #     p2 = get_object(chart, planet2)
    #     aspect = aspectType(p1, p2, aspect_names)
    #     return aspect #angle.distance(p1.lon, p2.lon)
    #
    # df['angle_test'] = df.apply(lambda row: angle_test(row, pos, 'Mars', 'Venus'), axis = 1)
    #
    # df['angle_test'].plot.area(subplots=True, figsize=(10,6), stacked=False, colormap='prism_r')

def purchase_a_computer(df, pos):

    '''
    Make sure that Mercury, the planet of communication, isn’t retrograde. Okay, I know I keep mentioning this influence. It’s always important, but there are times — I admit it — when you can bend the rules. Not in this case, though. Do not — I repeat, do not — purchase a computer (or a car) when Mercury is spinning backward.
    '''

    df['mercury_direct GO'] = df.apply(lambda row: must_be_direct(row, pos, 'Mercury', GO='GO'), axis = 1)
    df['mercury_direct NOGO'] = df.apply(lambda row: must_be_direct(row, pos, 'Mercury', GO='NOGO'), axis = 1)


    '''
    Make sure that Uranus and Mars aren’t doing anything unfortunate. High-tension squares, oppositions, and conjunctions, especially to Mercury or the Moon, are just the sort of thing you don’t want to see.
    '''

    df['UM_angle NOGO'] = df.apply(lambda row: auspicious_angle(row, pos, 'Uranus', 'Mercury', GO='NOGO'), axis = 1)
    df['UMoo_angle NOGO'] = df.apply(lambda row: auspicious_angle(row, pos, 'Uranus', 'Moon', GO='NOGO'), axis = 1)

    df['MM_angle NOGO'] = df.apply(lambda row: auspicious_angle(row, pos, 'Mars', 'Mercury', GO='NOGO'), axis = 1)
    df['MMoo_angle NOGO'] = df.apply(lambda row: auspicious_angle(row, pos, 'Mars', 'Moon', GO='NOGO'), axis = 1)

    '''
    Check that the Moon isn’t void-of-course.
    '''

    df['🌝_void NOGO'] = df.apply(lambda row: void_of_course(row, pos), axis = 1)

    '''
    Tip: It isn’t necessary, but an Aquarian influence — perhaps in the form of the Sun or Moon in that sign — makes sure that your technology is cutting edge.
    '''

    df['Sun_in_♒️ GO'] = df.apply(lambda row: sun_in(row, pos, 'Aquarius'), axis = 1)
    df['Moon_in_♒️ GO'] = df.apply(lambda row: moon_in(row, pos, 'Aquarius'), axis = 1)
