import itertools
from datetime import date, timedelta
import pandas as pd
import os

from flatlib.datetime import Datetime
from flatlib.chart import Chart
from flatlib import const, angle, props
from flatlib.aspects import hasAspect, getAspect, aspectType

aspect_names = props.aspect.name


def make_chart(date, pos):
    date = Datetime(date)
    return Chart(date, pos)

def get_object(chart, planet):
    for obj in chart.objects:
        if obj.id == planet:
            return obj

def calendar(duration):
    '''returns a calendar dataframe'''
    now = date.today()
    now_date = now.strftime('%Y/%m/%d')
    end = now + timedelta(days=duration)
    end_date = end.strftime('%Y/%m/%d ')

    range = pd.date_range(now_date, end_date)
    df = pd.DataFrame(index=range)
    df['date_string'] = df.index.strftime('%Y/%m/%d ')
    return df

def random_city():
    # Load data
    data_path  = os.environ['TMA_home'] + "/_data/GlobalAirportDatabase.csv"
    fields = ["City/Town", "Country", "Latitude Decimal Degrees", "Longitude Decimal Degrees"]
    all_cities = pd.read_csv(data_path, usecols=fields)

    # Select non-null Lat Lon
    cities = all_cities.loc[all_cities["Latitude Decimal Degrees"] != 0.]
    city = cities.sample(n=1) # pick one row at random

    city_name = city["City/Town"].values + ", " + city["Country"].values
    city_name = city_name[0]

    city_coords = (city.iloc[0]["Latitude Decimal Degrees"],
                city.iloc[0]["Longitude Decimal Degrees"])

    return city_name, city_coords

#
# ♑️♉️♍️ Astrology Functions
#

def must_be_direct(row, pos, planet, GO='GO'):
    '''
        check that planet is direct, not retrograde
    '''
    chart = make_chart(row['date_string'], pos)
    planet = get_object(chart, planet)
    movement = planet.movement()
    if GO == 'GO':
        if movement == "Direct":
            return planet.lonspeed
        else:
            return 0
    elif GO == 'NOGO':
        if movement != "Direct":
            return 1
        else:
            return 0

def moon_in(row, pos, sign):
    chart = make_chart(row['date_string'], pos)
    moon = get_object(chart, 'Moon')

    if moon.sign == sign:
        return 1
    else:
        return 0

def sun_in(row, pos, sign):
    chart = make_chart(row['date_string'], pos)
    sun = get_object(chart, 'Sun')

    if sun.sign == sign:
        return 1
    else:
        return 0

def new_moon(row, pos, moon_in = None):
    '''
        A New Moon, with the Sun and the Moon conjunct,
        classically signals a new beginning.
    '''
    chart = make_chart(row['date_string'], pos)
    day_after = row.name + timedelta(days = 1)
    date_after = day_after.strftime('%Y/%m/%d')
    next_chart = make_chart(date_after, pos)

    moon = get_object(chart, 'Moon')
    moon_phase = chart.getMoonPhase()
    next_moon_phase = next_chart.getMoonPhase()

    moon_score = 0

    if moon_phase != "First Quarter" and next_moon_phase == "First Quarter":
        moon_score += 1
        if moon_in:
            if moon.sign == moon_in:
                moon_score += 1
    return moon_score


def full_moon_buildup(row, pos):
    chart = make_chart(row['date_string'], pos)
    moon = get_object(chart, 'Moon')
    phase = chart.getMoonPhase()
    phases = ['Third Quarter','Last Quarter', 'First Quarter','Second Quarter']
    return phases.index(phase)


def waxing_moon(row, pos, moon_in = None):
    chart = make_chart(row['date_string'], pos)
    moon = get_object(chart, 'Moon')
    moon_phase = chart.getMoonPhase()
    sun = get_object(chart, 'Sun')
    if moon_in:
        if moon.sign == moon_in:
            if moon_phase in ['First Quarter', 'Second Quarter']:
                # maximum after new moon, decreases after that
                return 180 - angle.distance(sun.lon, moon.lon)
    elif moon_phase in ['First Quarter', 'Second Quarter']:
        # maximum after new moon, decreases after that
        return angle.distance(sun.lon, moon.lon)
    else:
        return 0

def waning_moon(row, pos, moon_in = None):
    chart = make_chart(row['date_string'], pos)
    moon = get_object(chart, 'Moon')
    moon_phase = chart.getMoonPhase()
    sun = get_object(chart, 'Sun')
    if moon_in:
        if moon.sign == moon_in:
            if moon_phase in ['Third Quarter', 'Last Quarter']:
                # maximum after new moon, decreases after that
                return 180 - angle.distance(sun.lon, moon.lon)
    elif moon_phase in ['First Quarter', 'Second Quarter']:
        # maximum after new moon, decreases after that
        return 180 - angle.distance(sun.lon, moon.lon)
    else:
        return 0



def auspicious_angle(row, pos, planet1, planet2, GO="GO"):
    date = Datetime(row['date_string'])
    chart = Chart(date, pos, IDs=const.LIST_OBJECTS) # IDs arg includes modern/outer planets in the chart
    p1 = get_object(chart, planet1)
    p2 = get_object(chart, planet2)

    aspect = aspectType(p1, p2, aspect_names)

    if GO == "GO":
        angles = [30, 60, 120]
    elif GO == "NOGO":
        angles = [45, 90, 180]

    if aspect in angles:
        return 1
    else:
        return 0


def count_trines_sextiles(row, pos, planets):
    '''
    count trines and sextiles between planets in array
    '''
    date = Datetime(row['date_string'])
    chart = Chart(date, pos, IDs=const.LIST_OBJECTS) # IDs arg includes modern/outer planets in the chart
    bodies = []
    for planet in planets:
        obj = get_object(chart, planet)
        bodies.append(obj)

    trines_sextiles = 0
    for a, b in itertools.combinations(bodies, 2):
        aspect = aspectType(a, b, aspect_names)
        if aspect in [60, 120]:
            trines_sextiles += 1

    return trines_sextiles


def void_of_course(row, pos):
    chart = make_chart(row['date_string'], pos)
    moon = get_object(chart, 'Moon')

    day_after = row.name + timedelta(days = 1)
    date_after = day_after.strftime('%Y/%m/%d')
    next_chart = make_chart(date_after, pos)
    next_moon = get_object(next_chart, 'Moon')

    if moon.sign != next_moon.sign:
        return 1
    else:
        return 0
