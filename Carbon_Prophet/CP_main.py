
# %% Imports
from datetime import timedelta
import datetime as dt
from sys import exit
import os
# data request and handling
import requests
import io
import pandas as pd
from scipy import stats
import numpy as np
# prediction
from fbprophet import Prophet
# visualisation
import matplotlib.pyplot as plt
from matplotlib import gridspec
import matplotlib.dates as mdates

#%%
save_path = os.environ['TMA_home'] + '/_website/_current_issue/'
print("=== 💀 CARBON PROPHET 🔮 ===")

# %%
# Load Mauna Loa observatory data
url = 'https://scrippsco2.ucsd.edu/assets/data/atmospheric/stations/flask_co2/daily/daily_flask_co2_mlo.csv'
col_names = ['sample_date', 'sample_time', 'excel_date', 'date', 'NC', 'FLG', 'Cx']

data = pd.read_csv(url, names=col_names, header=65)
parse_dates = lambda dates: pd.to_datetime(dates, format='%Y-%m-%d')
data['sample_date'] = data['sample_date'].apply(parse_dates)
print("📦 Data received")

#%% Check if new data is available
date_string = str(data.iloc[-1].date)

f = open(os.environ['TMA_home'] + '/Carbon_Prophet/_temp/last_date.txt', 'r')
last_date = f.readline()

if date_string == last_date:
    print("No new data is available")
    quit()
else:
    print("There is new data")

# %%
# reindex with one row per day, using nearest values to pad

df = data[['sample_date', 'Cx']].copy()
df.index = df['sample_date']
start, end = df['sample_date'].min(), df['sample_date'].max()
idx = pd.date_range(start, end)
df = df.reindex(idx, method='nearest')
df.reset_index(drop=True, inplace=True)

# %% Clean outliers
df['z'] = np.abs(stats.zscore(df['Cx']))
df.loc[df['z'] > 3, 'Cx'] = np.NaN
df = df.fillna(method='pad')
df = df.drop('z', axis=1)

#%% Prophet Setup

df.columns = ['ds', 'y']
time_span = 365 # length of prediction

m = Prophet()
print("Prophet.fit() ", m)
m.fit(df)

print("Prophet.predict()")
future = m.make_future_dataframe(periods=time_span)
forecast = m.predict(future)

#%% segment data

delta = pd.Timedelta(time_span, 'D')

d = future.iloc[-1]['ds'] - delta*2 # length of data to take: 2*delta (2*365 days)
data = df.loc[df.ds > d].copy()
data["y"] = data["y"].apply(pd.to_numeric)

d_futch = future.iloc[-1]['ds'] - delta # only take the futch part
futch = forecast.loc[forecast.ds > d_futch]

# %% Plotting

fig = plt.figure(figsize=(10, 10))
gs = gridspec.GridSpec(2, 1, height_ratios=[3, 1])
ax1 = plt.subplot(gs[0])

gs[0]

#fig_title = fig.suptitle("Carbon Prophet", fontsize=30)
#fig_title.set_family("Operator Mono")
title_font = {'fontname':'Inconsolata', 'fontsize':20}
axes_font = {'fontname':'Inconsolata', 'fontsize':13}

# label for last datapoint (red line)
last_date = df.iloc[-1]['ds'].strftime('%Y-%m-%d')
last_date_label = "Data ends: " + last_date

# plot
ax1.set_title("Last {0} days  |  Next {0} days".format(str(time_span)), **title_font)
ax1.plot(data['ds'], data['y'], color= 'k', linewidth=0.5, label="Mauna Loa CO² (PPM)")
ax1.fill_between(futch['ds'], futch['yhat_upper'], futch['yhat_lower'], color= (0,0,1,0.1), lw=0, label="Facebook Prophet")
ax1.plot(futch['ds'], futch['yhat'], color= (0,0,1,0.2), label='_nolegend_')
ax1.axvline(x=df.iloc[-1]["ds"], color= 'r', linewidth=0.5, label=last_date_label)
ax1.legend(loc="upper left", prop={'size':15, 'family':'Inconsolata'})

ax1 = plt.gca()
ax1.patch.set_facecolor('white')
ax1.tick_params(axis='both', which='both', labelsize=12)

# plot training data
df['y'] = df['y'].apply(pd.to_numeric)

ax2 = plt.subplot(gs[1], facecolor="white")
ax2.set_title("Training data: last {0} days".format(len(df)), **title_font)
ax2.plot(df['ds'], df['y'], color= 'k', linewidth=0.5)
ax2.tick_params(axis='both', which='both', labelsize=12)

# Plot Adjustments
# Logo and timestamp
# minotaur = ImageFont.truetype(font="minotaursanslombardic-light.otf", size=35)
# idraw.text((center_x+170,height-50), "Almanac.Computer", fill='black', font=minotaur)

plt.tight_layout()
plt.subplots_adjust(hspace=0.3, bottom=0.08)

plt.savefig(save_path + "Carbon_Prophet.svg", facecolor=fig.get_facecolor())
plt.close()

# plt.show()

print()


# %% Save the date
f = open(os.environ['TMA_home'] + '/Carbon_Prophet/_temp/last_date.txt', 'w')
f.write(date_string)
f.close()

# %%
