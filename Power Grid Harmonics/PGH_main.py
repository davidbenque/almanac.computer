# %%

import requests
from io import StringIO
from datetime import datetime, timedelta
import os

import pandas as pd
import numpy as np
from scipy.signal import argrelextrema

import matplotlib
matplotlib.use('Agg')

import matplotlib.pyplot as plt
from matplotlib import font_manager as fm, rcParams


from harmonic_functions import *

# %%
'''
Elexon Portal/BMRS: https://www.bmreports.com/bmrs/

BMRS API guide: https://www.elexon.co.uk/wp-content/uploads/2017/06/bmrs_api_data_push_user_guide_v1.1.pdf
p.66

https://api.bmreports.com/BMRS/FREQ/<VersionNo>?APIKey=<APIKey>&FromDateTime=<FromDateTime>&ToDateTime=<ToDateTime>&ServiceType=<xml/XML/csv/CSV>

Default Value (if none specified):
From DateTime = Current System DateTime – 48 Hr (configurable)
To DateTime = Current System DateTime
'''

print("=== POWER GRID HARMONICS ===")

save_path = os.environ['TMA_home'] + '/_website/_current_issue/'

key = os.environ['BMRS_key']


# %%
# start and end time stamps

now = datetime.now()
start_time = now - timedelta(days=1)

now_string = now.strftime('%Y-%m-%d %H:%M:%S')
start_string = start_time.strftime('%Y-%m-%d %H:%M:%S')

print("Request data from " + start_string + " to " + now_string)

# %%
# combine request

url = "https://api.bmreports.com/BMRS/FREQ/v1?APIKey="
st = "&FromDateTime=" + start_string
end = "&ToDateTime=" + now_string

r = requests.get(url + key + st + end + "&ServiceType=csv")
print("📦 received")
# %%
# read the received data
df = pd.read_csv(StringIO(r.text))
df = df.drop(['FTR'])

# notebook: show response
# df.head()


# %%
# data processing
df['HDR'] = pd.to_datetime(df['HDR'], format='%Y%m%d%H%M%S')
df.rename(columns={'HDR': 'timestamp', 'SYSTEM FREQUENCY DATA':'frequency'}, inplace=True)
df.index = df['timestamp']
del df['timestamp']

# notebook show refactored time-series
#df.head()
print("datapoints: ", df.values.shape)


# %%
# code from FOREX Harmonic Pattern Scanning Algorithm in Python: Introduction
# https://www.youtube.com/watch?v=tGpMb8R9D9M

max_idx = list(argrelextrema(df.values, np.greater, order=10)[0])
min_idx = list(argrelextrema(df.values, np.less, order=10)[0])
idx = max_idx + min_idx
idx.sort()
peaks = df.values[idx]

# notebook show peaks
# plt.plot(df.values)
# plt.scatter(idx, peaks, c='r')
# plt.show()

# %%
err_allowed = 5.0/100

graph_n = 0

plot = {
    "label": "",
    "x": "",
    "values": "",
    "idx": "",
    "pat": "",
    "ax labels": ""
}

for i in range(100, len(df)):

    current_idx, current_pat, start, end = peak_detect(df.values[:i], order=8)

    XA = current_pat[1] - current_pat[0]
    AB = current_pat[2] - current_pat[1]
    BC = current_pat[3] - current_pat[2]
    CD = current_pat[4] - current_pat[3]

    moves = [XA, AB, BC, CD]

    gart = is_gartley(moves, err_allowed)
    butt = is_butterfly(moves, err_allowed)
    bat = is_bat(moves, err_allowed)
    crab = is_crab(moves, err_allowed)

    harmonics = np.array([gart, butt, bat, crab])
    labels = ['Gartley 📐', 'Butterfly 🦋', 'Bat 🦇', 'Crab 🦀']

    if np.any(harmonics == 1) or np.any(harmonics == -1):
        for index, j in np.ndenumerate(harmonics):
            if j == 1 or j == -1:
                sense = '🐮 Bullish ' if j == 1 else '🐻 Bearish '
                label = sense + labels[index[0]]
                print("spotted a: " + label)

                plot["label"] = label
                plot["x"] = np.arange(start, i+15)
                plot["values"] = df.values[start:i+15]
                plot["idx"] = current_idx
                plot["pat"] = current_pat
                plot["ax labels"] = df.index[current_idx]

# %%


# save Title
with open(save_path+"PGH_title.js", 'w', encoding='utf8') as file:
    title = 'var PGH_title = "' + plot["label"] + '";'
    file.write(title)
    file.close()

print('---')
print('processing most recent pattern')
print('exported title: ' + plot["label"] )

# Draw and save chart

font_path = os.environ['TMA_home'] + '/_data/fonts/Inconsolata-Medium.ttf'
fpath = os.path.join(rcParams["datapath"], font_path)
prop = fm.FontProperties(fname=fpath)

fig = plt.figure(figsize=(10,10))

plt.plot(plot["x"], plot["values"], c='k', linewidth=0.5) # frequency
plt.plot(plot["idx"], plot["pat"], c='r', linewidth=1) # pattern

plt.xticks(plot["idx"], plot["ax labels"], rotation='vertical', fontsize=20, fontproperties=prop)
plt.yticks(fontsize=20, fontproperties=prop)
plt.ylabel("UK National Grid Frequency (Hz)\n", fontsize=20, fontproperties=prop)
plt.grid(True, c='b', linewidth=0.1)

plt.tight_layout()

plt.savefig(save_path + "PGH.svg", facecolor=fig.get_facecolor())
print('exported chart')

# %%
