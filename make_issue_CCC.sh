#!/bin/bash

# Issue script to run from laptop, CD'd in the repo

# XXX
#source /home/pi/berryconda3/bin/activate /home/pi/berryconda3
# XXX
#cd /home/pi/TMA_checkout/

python Cover/cover_meta.py
python Cover/dial-gen-date.py
python Cover/dial-gen-earth.py
python Cover/dial-gen-stocks.py
python Cover/dial-gen-sun.py

python "Cosmic Commodity Chart/CCC_main.py"
python "Crisis Proximity Index/CPI_table.py"
python "Carbon_Prophet/CP_main.py"
# Hashgets run at the end of the day in a different cron job
# python HASHGETS/hashget.py
python "r_astrologymemes/main.py"
python "Lotto/lotto_pred.py"
python "Electional Astrology/EA_main.py"
python "Power Grid Harmonics/PGH_main.py"

# archive new issue
time_stamp=$(date +%Y_%m_%d_%H_%M_%S)
cp -r _website/_current_issue/. _archive/$time_stamp

# commit issue
#cd /home/pi/TMA_repo.git/
git add --a
time_stamp=$(date +%Y_%m_%d)
git commit --m "issue $time_stamp"

# push to Gitlab
#HOME=/home/pi git push gitlab master
git push Gitlab master

