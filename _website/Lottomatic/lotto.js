var p = d3.select(".lotto_numbers").selectAll("div")
  .data(lotto_preds)
  .enter()
  .append("div")
  .attr("class", "draw")
  .html(function (d, n) {
      var len = d.length;
      var draw = ''
      for (var i = 0; i < len; i++) {
        draw += '<span class="number_ball">' + d[i] + '</span>';
      };
      return "Draw " + (n+1) + ": " + draw;
  }
 );
