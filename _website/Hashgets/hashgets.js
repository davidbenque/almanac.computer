/**
 * @Author: davidbenque
 * @Date:   2017-11-13 3:54:38
 * @Last modified by:   davidbenque
 * @Last modified time: 2018-05-03 3:12:04
 */



var data = d3.csv('_current_issue/hashgets.csv',
  function(d) {
    return {
      type: d.get_type,
      hash: d.hash,
      hash_get: d.hash_get,
      date: d.date,
      size: d.size
    };
  },
  function(data) {
    var hashgets = d3.select('.hashgets');

    var get_divs = hashgets.selectAll('div')
      .data(data)
      .enter()
      .append('div')
      .html(function(d){
        var timestamp = '<div class="timestamp">' + d.date + '<br>size: ' + d.size + '</div>'
        var hash_num = function(d){
          if(d.type == 'DUBS'){
            return '  …' + d.hash.substr(d.hash.length - 12)
          } else{ return d.hash}
        }
        var hash = '<div class="hash">' + hash_num(d) + '<span class="get-hilight">' + d.hash_get + '</span></div>';
        var get_type = function(d){
          var icons = ['💣','💰','🎉','🎁','😱','👍','👌','♥️','♣️','♦️','♠️'];
          var icon = icons[Math.floor(Math.random() * icons.length)];
          if(d.type == 'DUBS'){return d.type} else {return d.type + ' ' + icon.repeat(d.hash_get.length)}
        }

        var title = '<div class="type">' + get_type(d) + '</div>';
        return title + hash + timestamp
      })
      .attr('class', function(d){ return 'hashget '  + d.type})


  });
