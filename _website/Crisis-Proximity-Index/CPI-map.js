/**
 * @Author: davidbenque
 * @Date:   2017-10-29 11:30:35
 * @Last modified by:   davidbenque
 * @Last modified time: 2018-05-03 2:54:46
 */



var width = 950,
    height = 640;

var projection = d3.geo.eisenlohr()
    .scale(75)
    .translate([width / 2, height / 2])
    .precision(.1);

var path = d3.geo.path()
    .projection(projection);

var graticule = d3.geo.graticule();

var svg = d3.select(".crisis-map").append("svg")
    .attr("width", width)
    .attr("height", height)
    .attr("viewBox", "0 0 950 640")
    .attr("preserveAspectRatio","xMinYMin meet");

svg.append("defs").append("path")
    .datum({type: "Sphere"})
    .attr("id", "sphere")
    .attr("d", path);

svg.append("use")
    .attr("class", "stroke")
    .attr("xlink:href", "#sphere");

svg.append("use")
    .attr("class", "fill")
    .attr("xlink:href", "#sphere");

svg.append("path")
    .datum(graticule)
    .attr("class", "graticule")
    .attr("d", path);

d3.json('_current_issue/CPI_geo.json', function(error,data){
    if (error) { return error; };

    svg.selectAll('path.planet').data(data.features)
                .enter()
                .append('path')
                .attr('class', 'planet')
                .attr('class', function(d){
                  var classes = '';
                  classes += ' ' + d.geometry.type;
                  if(d.properties.date == 'today'){if (d.properties.diff > 0){
                    classes += ' diff_pos '
                  } else { classes += ' diff_neg '}}
                return classes })
                .attr('d', path);


})
