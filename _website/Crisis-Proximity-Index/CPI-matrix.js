/**
 * @Author: davidbenque
 * @Date:   2017-10-25 1:14:46
 * @Last modified by:   davidbenque
 * @Last modified time: 2018-05-03 2:55:03
 */



// Today's date

var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1; //January is 0!
var yyyy = today.getFullYear();

if(dd<10) {
    dd = '0'+dd
}

if(mm<10) {
    mm = '0'+mm
}

today = yyyy + '-' + mm + '-' + dd;



var data = d3.csv('_current_issue/CPI_table.csv',
  function(d) {
    return {
      date: d.date,
      mercury: Math.round((+d.mercury_dist + 0.00001) * 100) / 100,
      mercury_diff: +d.mercury_diff,
      venus: Math.round((+d.venus_dist + 0.00001) * 100) / 100,
      venus_diff: +d.venus_diff,
      mars: Math.round((+d.mars_dist + 0.00001) * 100) / 100,
      mars_diff: +d.mars_diff,
      jupiter: Math.round((+d.jupiter_dist + 0.00001) * 100) / 100,
      jupiter_diff: +d.jupiter_diff,
      total: Math.round((+d.total + 0.00001) * 100) / 100,
      total_diff: +d.total_diff

    };
  },
  function(data) {

// color scale
color = d3.scale.linear().domain([-1,0,1])
      .interpolate(d3.interpolateRgb)
      .range([d3.rgb("#ff0000"), d3.rgb('#FFFFFF'), d3.rgb('#00ff00')]);

    var table = d3.select('.crisis-matrix-table')
      .append('table')
      .attr('class', 'table');

    var header = table.append('thead')
      .html('<td> </td><td>☿</td><td>♀</td><td>♂</td><td>♃</td><td>CPI</td>');

    var tbody = table.append('tbody');

    var rows = tbody.selectAll('tr')
      .data(data)
      .enter()
      .append('tr')
      .attr('class', function(d){
        if(d.date == today){
          return 'today'
        }
      });

    var columns = ['date', 'mercury', 'venus', 'mars', 'jupiter', 'total']
    var cells = rows.selectAll('td')
      .data(function (row) {
        return columns.map(function (column) {
          if(column != 'date'){
            var diffCol = column + "_diff";
            return {column: column, value: row[column], diff:row[diffCol]};
          } else {
            return {column: column, value: row[column]};
          }
        });
      })
      .enter()
      .append('td')
      .text(function(d){
        var indicator = '';
        if(d.diff > 0){ indicator = "⬆ "};
        if(d.diff < 0){ indicator = "⬇ "}
        return indicator + d.value;
      })
      .attr('style', function(d){
        if(d.column != 'date'){
            return 'background-color: ' + color(d.diff);
        }
      });
  }
);
