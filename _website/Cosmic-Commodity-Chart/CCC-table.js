

var data = d3.csv('_current_issue/CCC_peaks_valleys.csv',
  function(d) {
    return {
      direction: +d.direction,
      date: d.date,
      price: "$" + d.pred_price
    };
  },
  function(data) {
    var table = d3.select('.cosmic-chart-table')
      .append('table')
      .attr('class', 'table')
    var thead = table.append('thead')
      .html('<td></td><td>Opportunities</td><td></td>')
    var	tbody = table.append('tbody');



    // create a row for each object in the data
    var rows = tbody.selectAll('tr')
      .data(data)
      .enter()
      .append('tr')
      .attr('class', function(d){
          if (d.direction > 0) {
              return "green";
          } else {
              return "red";
        }});

    // create cells
    var columns = ['direction', 'date', 'price'];
    var cells = rows.selectAll('td')
		  .data(function (row) {
		    return columns.map(function (column) {
		      return {column: column, value: row[column]};
		    });
		  })
		  .enter()

		  .append('td')
        .html(function(d){
          if(d.column != 'direction'){
            return d.value;
          } else {
            if(d.value == 1){
              return '<img src="Cosmic-Commodity-Chart/_images/peak.svg">'
            } else { return '<img src="Cosmic-Commodity-Chart/_images/valley.svg">'}
          }
        });

      }
  );
