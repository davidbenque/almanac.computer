

function make_dial(target, data_file, label_x=15, label_y=0, radius_step=20, symbol="X"){
var width = 300;
var height = 300;

var dial_svg = d3.select(target)
  .append("svg")
.attr("width", width)
.attr("height", height)
  .attr("class", "dial")
  .attr("viewBox", "0 0 300 300")
  .attr("preserveAspectRatio","xMinYMin meet");

  // filters go in defs element
  var defs = dial_svg.append("defs");

  // create filter with id #drop-shadow
  // height=130% so that the shadow is not clipped
  var filter = defs.append("filter")
      .attr("id", "drop-shadow")
      .attr("height", "130%");

    filter.append("feColorMatrix")
      .attr("type", "matrix")
      .attr("values", "0 0 0 0.2 0 0 0 0 0 0  0 0 0 0 0  0 0 0 1 0")
      .attr("result", "color");

    filter.append("feGaussianBlur")
        .attr("in", "color")
        .attr("stdDeviation", 4)
        .attr("result", "blur");

    filter.append("feOffset")
        .attr("in", "blur")
        .attr("dx", 0)
        .attr("dy", 0)
        .attr("result", "offsetBlur");

    var feMerge = filter.append("feMerge");

    feMerge.append("feMergeNode")
        .attr("in", "offsetBlur")
    feMerge.append("feMergeNode")
        .attr("in", "SourceGraphic");

// dial_svg.append("rect")
//   .attr("x", 0).attr("y", 0)
//   .attr("class", "background")
//   .attr("width", width)
//   .attr("height", height);

var data = d3.csv(data_file, function(error,data){
      if (error) { return error; };

      var dial_disc = dial_svg.selectAll('circle').data(data)
              .enter()
              .append("g")
              .attr("class", "dial_disc");

      var max_radius = 120;

      var tick_size = 15;
      var tick_size_s = 7;

      dial_disc.append('circle')
              .attr('class', 'circle')
              .attr("cx", 0)
              .attr("cy", 0)
              .attr("r", function(d,i){return max_radius - (radius_step*i)})
              .style("filter", "url(#drop-shadow)");

      var tick = d3.svg.arc()
        .innerRadius(function(d,i){return (max_radius - tick_size) - (radius_step*i)})
        .outerRadius(function(d,i){return max_radius - (radius_step*i)})
        .startAngle(0)
        .endAngle(0);

      var small_tick = d3.svg.arc()
        .innerRadius(function(d,i){return (max_radius - tick_size_s) - (radius_step*i)})
        .outerRadius(function(d,i){return max_radius - (radius_step*i)})
        .startAngle(0)
        .endAngle(0);

      for (var i = 0; i <= 45; i++) {
        dial_disc.append("path").attr({
          class: "small_tick",
          d: small_tick,
          transform: "rotate(" + i*8 + ")"
        });
      };

      var circle_ticks = dial_disc
        .append("path").attr({
          class: "tick",
          d: tick
        });

      d3.selectAll(".dial_disc")
        .attr("transform", function(d){
          var circular_scale = d3.scale.linear().domain([d.min, d.max]).range([0, 360]);
          return "translate(150,150) rotate(" + circular_scale(d.value) + ") " } );

      var label = dial_svg.append("g")
        .attr({
          class: "dial_label",
          transform: "translate(" + label_x + "," + label_y + ")"
        });

      var initals = label.selectAll('g')
      .data(data)
      .enter()
      .append("text")
      .text(function(d){return d.name})
      .attr("y", function(d,i){return i*20})
      .attr("font-size", "20px")
      .attr("text-anchor", "middle");

      var center_symbol = dial_svg.append("text")
        .text(symbol)
        .attr("font-size", "40px")
        .attr("x", 150)
        .attr("y", 155)
        .attr("text-anchor", "middle")
        .attr("alignment-baseline", "middle");

  });
}
