
function EA_table(csv, div, title){
  var data = d3.csv('_current_issue/' + csv,
    function(d) {
      return {
        date: d.date,
        score: Number.parseFloat(d.score).toFixed(3)
      };
    },
    function(data) {
      var table = d3.select(div)
        .append('table')
        .attr('class', 'table')
      var thead = table.append('thead')
        .html('<td>' + title + '</td><td>Score</td>')
      var	tbody = table.append('tbody');

      // create a row for each object in the data
      var rows = tbody.selectAll('tr')
        .data(data)
        .enter()
        .append('tr');

      // create cells
      var columns = ['date', 'score'];
      var cells = rows.selectAll('td')
  		  .data(function (row) {
  		    return columns.map(function (column) {
  		      return {column: column, value: row[column]};
  		    });
  		  })
  		  .enter()
  		  .append('td')
          .html(function(d){return d.value;});
        }
    );
}
