
var data = d3.json('_current_issue/astromeme_data.json',
    function(data) {
        var container = d3.select('.astromeme');
        container.append('h2')
            .text(data.title);
        var details = container.append('div');
        details.append('a')
            .text('🔗 /r/astrologymemes')
            .attr('href', data.link);
        details.append('br');
        details.append('span')
            .html('by: ' + data.author + ' - Score: ' + data.score);
        container.append('img')
            .attr('src', '_current_issue/astromeme.jpg')
    });