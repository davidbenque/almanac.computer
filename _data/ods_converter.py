# -*- coding: utf-8 -*-
# @Author: davidbenque
# @Date:   2017-05-08 16:30:31
# @Last Modified by:   davidbenque
# @Last Modified time: 2017-05-08 16:42:53

import pyexcel as pe

data = pe.get_book(file_name="commodityprices-banana-5may17.ods")
data.save_as("converted.xls")