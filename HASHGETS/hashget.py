
# %%

import requests
import time
import json
import re
import datetime
import csv
import os

# %% 

print("Starting HASHGETS")

current_milli_time = int(round(time.time() * 1000))
r = requests.get('https://blockchain.info/blocks/' + str(current_milli_time) + "?format=json")
json_data = json.loads(r.text)

# %% 

get_names = {2:'DUBS', 3:'TRIPS', 4:'QUADS', 5:'QUINTS', 6:'SEXTS', 7:'SEPTS', 8:'OCTS', 9:'NONS'}

# search for consecutive repeated characters at the end of the hash string
# this will only catch TRIPS and up
reg = re.compile(r"(\w)\1{1,}$")

print(len(json_data['blocks']), ' blocks')

matches = []

# %%

for block in json_data['blocks']:

    match = re.search(reg, block['hash'])
    #print(block['hash'])

    if match:
        hash_main = block['hash'][:match.span()[0]]
        hash_get = block['hash'][match.span()[0]:]

        match_r = requests.get('https://blockchain.info/rawblock/' + str(block['hash']))
        match_block = json.loads(match_r.text)
        block_time = datetime.datetime.fromtimestamp(match_block['time'])
        get_type = get_names[len(hash_get)]
        block_size = str(match_block['size'])

        match_data = {
            'get_type': get_type,
            'hash': hash_main,
            'hash_get': hash_get,
            'date': block_time,
            'size': block_size}
        matches.append(match_data)
        print(get_type)
        print(hash_main, ' | ', hash_get)
        print(block_time, 'size: ' + block_size)

# %%

output = os.environ['TMA_home'] + '/_website/_current_issue/hashgets.csv'

if matches:
    with open(output, 'w') as csvfile:
        colnames = ['get_type', 'hash', 'hash_get', 'date', 'size']
        writer = csv.DictWriter(csvfile, fieldnames = colnames)
        writer.writeheader()
        for match in matches:
            writer.writerow(match)

print("# END HASHGETS #")


# %%
