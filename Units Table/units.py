from astropy import units as u
from astropy.units import imperial

from operator import itemgetter
from tabulate import tabulate

# %%

def make_table(base, units):
    table = []
    for unit in units:
        conv = base.to(unit)
        table.append([conv.value, conv.unit])
    table = sorted(table, key=itemgetter(0), reverse=True)
    table.insert(0, [base.value, base.unit])
    print(tabulate(table,  tablefmt="html"))

# %%

length_base = 1 * u.meter
length_units = [u.nm, u.mm, u.cm, u.km, u.earthRad, u.solRad, u.AU, u.lightyear, u.parsec, imperial.inch, imperial.foot, imperial.fur, imperial.mile]

make_table(length_base, length_units)

# %%

weight_base = 1 * u.kg
weight_units = [u.ng, u.mg, u.cg, u.M_e, u.M_p, u.earthMass, u.jupiterMass, u.solMass, u.u, imperial.lb, imperial.oz, imperial.slug, imperial.st]

make_table(weight_base, weight_units)

# %%

time_base = 1 * u.min
time_units = [u.sday, u.wk, u.h, u.s, u.yr, u.fortnight]

make_table(time_base, time_units)
