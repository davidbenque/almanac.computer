#%%
import pandas as pd
import datetime

import os


# %% directories
parent_path = os.environ['TMA_home']

data_dir = parent_path + "/_data/"
issue_output = parent_path + "/_website/_current_issue/"
temp_output = parent_path + "/Cosmic Commodity Chart/_temp/"
CCC_dir = parent_path + "/Cosmic Commodity Chart/"

# %% Log in to Quandl API
import quandl

with open(data_dir + "quandl-key.txt", "r") as text:
    for line in text:
        quandl_key = line
quandl.ApiConfig.api_key = quandl_key

# %% Query elements

commodities = {
    "Corn": "CHRIS/CME_C1",
    "Wheat": "CHRIS/CME_W3",
    "Soybean": "CHRIS/CME_S3",
    "Rough Rice": "CHRIS/CME_RR1", # only goes back to 1986
    "Cotton No.2": "CHRIS/ICE_CT2",
    "Sugar": "CHRIS/ICE_SB2",
    "Orange Juice": "CHRIS/ICE_OJ4",
    "Coffee C": "CHRIS/ICE_KC3",
    # Livestock
    "Live Cattle": "CHRIS/CME_LC3",
    "Feeder Cattle": "CHRIS/CME_FC2",
    "Lean Hog": "CHRIS/CME_LN4",
    # energy
    "Crude Oil": "CHRIS/CME_CL38",
    # Metals
    "Copper": "CHRIS/CME_HG2",
    # Precious
    "Gold": "CHRIS/CME_GC5",
    "Silver": "CHRIS/CME_SI7",
    }

planet_lists = {
    "Full": ['mercury', 'venus', 'earth', 'mars',
            'jupiter', 'saturn', 'uranus', 'neptune', 'pluto'],
    "Inner": ['mercury', 'venus', 'earth', 'mars']
}

# outer planets make bad charts
# "Outer": ['jupiter', 'saturn', 'uranus', 'neptune', 'pluto']

lengths = [365, 730]

# %% Query
from random import choice

plans = list(planet_lists.keys())
coms = list(commodities.keys())

query = [choice(plans), choice(coms), choice(lengths)]
print("CCC starting: {0} {1} Chart, {2} Days".format(*query))

# %% export meta-data file
with open(temp_output + "CCC_title.txt", "w") as file:
    file.write(str(datetime.datetime.now()) + "\n")
    for i in query:
        file.write(str(i) + "\n")
    file.write(str(commodities[query[1]]) + "\n")
    file.write(str(planet_lists[query[0]]))
    file.close()


# %%  export title file
from symbols import *

planets_symbol_set = []
for i in planet_lists[query[0]]:
    planets_symbol_set.append(planet_symbols[i])

planets_symbol_set

with open(issue_output + "CCC_title.js", "wb") as file:
    file.write("var CCC_title = '".encode('utf8'))
    title = "{0} {1} Chart".format(query[0], query[1]) + "';\n"
    file.write(title.encode('utf8'))
    planets = "["
    for s in planets_symbol_set:
        planets += s
    planets += "] "
    file.write("var CCC_descr = '".encode('utf8'))
    planets.encode('utf8')
    file.write(planets.encode('utf8'))
    com_symbol = commodities_symbols[query[1]] + " "
    file.write(com_symbol.encode('utf8'))
    days = str(query[2]) + "Days';"
    file.write(days.encode('utf8'))
    file.close()

# copyfile(temp_output + "CCC_title.txt", issue_output + "CCC_title.txt")


# %% Historical data

today = datetime.date.today()
start_date = today - datetime.timedelta(days = 10950) # 30 years ago

price_data_30y = quandl.get(commodities[query[1]], start_date=start_date, end_date=today)

# %% Set up planets

# load skyfield and ephemeris
from skyfield.api import load, Loader
from astropy import units as u

# load Skyfield data
try:
    load = Loader(data_dir + '/skyfield')
    planets = load('de421.bsp')
    ts = load.timescale()
    print("Skyfield Loaded")
except:
    print('\033[93m' + "Skyfield not loaded"
      + '\x1b[0m')


# list of planets for current query
planet_list = planet_lists[query[0]]

# pre-exported list of orbit min and max for all planets

planets_minmax = pd.read_csv(CCC_dir + "planets_minmax.csv", index_col = 0)

def add_planet_positions(df, planet_list):
    '''
    input: dataframe with datetime index, list of planets
    output: dataframe with planet distance (barycentric) columns
            added for the planets in planet_list and dates in df.index
    '''
    for planet in planet_list:
        query_name = planet + " barycenter"
        col_name = planet + "_dist"
        p = planets[query_name]
        df[col_name] = p.at(ts.utc(df.index)).distance().to(u.km)
    return df

def orbit_scaler(planet_dist, x):
    min_dist, max_dist = planets_minmax.loc[planet_dist]
    return ((x - min_dist)/(max_dist-min_dist))


# %% Get training data
import pytz
from pytz import timezone

training_data = price_data_30y["Settle"].to_frame()
training_data["Date"] = training_data.index

utc = timezone('UTC')
def to_utc(x):
    # add utc timezone to datetime index
    a = x.replace(tzinfo=pytz.utc)
    return a

training_data["Date"] = training_data["Date"].apply(to_utc)
training_data.set_index(['Date'], inplace=True)
training_data = training_data.rename(columns={"Settle":"Price"})
training_data = add_planet_positions(training_data, planet_list)

target = training_data["Price"]
features = training_data.drop(["Price"], axis=1)

# feature scaling
features_scaled = pd.DataFrame()
for column in features:
    features_scaled[column] = orbit_scaler(column, features[column])

print("Training data: features ", features.shape, "target ", target.shape )

# %% Training

from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(
    features_scaled.values, target.values, test_size=0.3, random_state=0)

from sklearn import svm

clf = svm.SVR(kernel='rbf')
clf.fit(X_train, y_train)
score = clf.score(X_test, y_test)

print("Model score: ", score)

# %% Prediction

tomorrow = datetime.date.today() + datetime.timedelta(days=1)
end_date = tomorrow + datetime.timedelta(days=query[2])
dates = pd.date_range(tomorrow, end_date, freq = 'D', tz='utc')
dates = pd.to_datetime(dates)
future = pd.DataFrame(index=dates, columns=['A'])
future = add_planet_positions(future, planet_list)
future.drop(["A"], axis=1, inplace=True)

# scale
for column in future:
    future[column] = orbit_scaler(column, future[column])

# predict price
future["pred_price"] = clf.predict(future.values)

# export
future["pred_price"].to_csv(temp_output + "pred_price.csv", header=False)

print("Predictions: Done")

# %% Peaks and Valleys

future["change"] = future["pred_price"] - future["pred_price"].shift(-1)
price_preds = future[["pred_price", "change"]].copy()
price_preds = price_preds.fillna(0)
price_preds['direction'] = price_preds.change.map(lambda x: 0 if x == 0 else x/abs(x))
price_preds['dir_change'] = (price_preds.direction != price_preds.direction.shift(1)).astype(int)

# remove change flag for first and last dates
for date in [0, -1]:
    price_preds.iat[date, 3] = 0 # set dir_change to 0

price_preds["block"] = price_preds.dir_change.cumsum()

pd.options.mode.chained_assignment = None # remove weird warning for now

#select rows with a direction change
peaks_valleys = price_preds.loc[price_preds["dir_change"] == 1]

peaks_valleys.drop(["change","dir_change", "block"], axis=1, inplace=True)

# change date index to column
peaks_valleys["date"] = peaks_valleys.index
peaks_valleys = peaks_valleys.reset_index(drop=True)

# export csv wihtout the index column
peaks_valleys.to_csv(temp_output + "peaks_valleys.csv", index=False)

# Format date and price for display export
PV_display = peaks_valleys.copy()
PV_display["date"] = PV_display["date"].apply(lambda x: datetime.datetime.strftime(x, '%Y-%m-%d'))
PV_display["pred_price"] = PV_display["pred_price"].apply(lambda x: '%.2f'%(x))
PV_display.to_csv(issue_output + "CCC_peaks_valleys.csv", index=False)

print("Exported peaks and valleys")


# %% Chart Layers

from CCC_chart_layers import chart_planets, chart_preds

chart_planets(future, planet_list, temp_output)
chart_preds(future, peaks_valleys, price_preds, temp_output)
print("Exported chart layers")

# %% Export final Chart
from CCC_final_chart import final_chart
final_chart()
print("Exported final chart")
print("# END CCC #")


# %%
