planet_symbols = {
    "mercury": "☿",
    "venus": "♀️",
    "earth": "♁",
    "mars": "♂",
    "jupiter":"♃",
    "saturn":"♄",
    "uranus":"⛢",
    "neptune":"♆",
    "pluto":"♇"}

# Commodity Symbols

commodities_symbols = {
    "Corn":"🌽",
    "Wheat":"🌾",
    "Soybean": "🌾S (CME)",
    "Rough Rice": "🌾RR (CME)",
    "Cotton No.2": "CT (ICE)",
    "Sugar": "SB (ICE)",
    "Orange Juice": "🍊OJ (ICE)",
    "Coffee C": "☕️KC (ICE)",
    "Live Cattle": "🐄LE",
    "Feeder Cattle": "🐄FC",
    "Lean Hog": "🐖HE",
    "Crude Oil": "CL (CME)",
    "Copper":"HG (CME)",
    "Zinc": "ZN (SHFE)",
    "Gold": "🥇GC (CME)",
    "Silver": "🥈SI (CME)"
    }
