from PIL import Image, ImageDraw, ImageFont
import pandas as pd
import datetime
import os

def final_chart():
    # %%
    width = 1080 # dim of the matplotlib PDFs
    height = 1400

    center_x = width/2
    center_y = (width/2) + (height - width)

    # %%

    path = os.environ['TMA_home'] + '/Cosmic Commodity Chart/_temp/'
    preds = pd.read_csv(path + 'pred_price.csv', names = ["date", "price"])

    # %%

    first_date = preds.iloc[0]["date"].split(" ")[0]
    first_day = datetime.datetime.strptime(first_date, "%Y-%m-%d")
    first_price = round(float(preds.iloc[0]["price"]), 2)

    last_date = preds.iloc[-1]["date"].split(" ")[0]
    last_day = datetime.datetime.strptime(last_date, "%Y-%m-%d")
    last_price = round(float(preds.iloc[-1]["price"]), 2)


    # %%

    def price_flag(date, price, side, up=None, diff=None):
        '''
        draw price boxes at the top
        date: a datetime
        price: float
        side: "L" or "R"
        up: Boolean
        diff: price difference float
        '''

        w = 400
        h = 140
        y = ((height - width)/2) - (h/2)

        if side == "L":
            x = center_x - w
        else:
            x = center_x
        if side == "L":
            if up == True:
                y -= 30
            else:
                y += 30

        if up == True:
            color = (0,255,0) # green
        elif up == False:
            color = (255, 0, 0) # red
        else:
            color = 'black'


        idraw.rectangle((x, y, x + w, y + h), fill=None, outline="black",)
        date_string = date.strftime("%Y-%m-%d")
        idraw.text((x + 10, y + 5), date_string, fill='black', font=operator_date)


        sign = ""
        if diff:
            if diff > 0:
                sign = "+"
            if diff < 0:
                sign = "-"
            price_diff = sign + "$" + str(round(abs(diff),2))
            idraw.text((x + 10, y + 150), price_diff, fill=color, font=operator_b_diff)


        price_tag = "$" + str(price)
        idraw.text((x + 10, y + 30), price_tag, fill=color, font=operator_b_price)

    # %% fonts
    font_path = os.environ['TMA_home'] + '/_data/fonts/'
    operator_date = ImageFont.truetype(font= font_path + "OperatorMono-Book.ttf", size=30)
    operator_b_diff = ImageFont.truetype(font= font_path + "OperatorMono-Bold.ttf", size=50)
    operator_b_price = ImageFont.truetype(font= font_path + "OperatorMono-Bold.ttf", size=90)
    calibri = ImageFont.truetype(font= font_path + "Calibri.ttf", size=80)
    #minotaur = ImageFont.truetype(font="minotaursanslombardic-light.otf", size=35)

    # %% set up image
    img = Image.new('RGBA', (width,height))
    idraw = ImageDraw.Draw(img)

    #background
    idraw.rectangle((0,0,width,height), fill="white")


    # %% Matplotlib Layers

    planets_lyr = Image.open( path + 'planets.png')
    preds_lyr = Image.open(path + "pred_price.png")

    for layer in [planets_lyr, preds_lyr]:
        #layer = layer.thumbnail((1080,1080), Image.ANTIALIAS)
        img.paste(layer, (-12,height-width), layer)


    # %% Price Labels and center line

    price_flag(first_day, first_price, "R")

    is_up = first_price < last_price
    price_flag(last_day, last_price, "L", up=is_up, diff=last_price - first_price)

    # center line and arrow
    up_offset = 60 if is_up else 90
    idraw.line([(center_x, (height-width)+40),(center_x,up_offset)], fill='black', width=5)
    idraw.line([(center_x, center_y),(center_x,430)], fill='black', width=5)

    idraw.text((center_x-5,(height-width)-60), "→", fill='black', font=calibri)

    # %% description
    chart_info = open(path + "CCC_title.txt").readlines()
    chart_title = chart_info[4].rstrip()

    chart_title

    idraw.text((10,height-50), chart_title, font=operator_date, fill='black')


    # %% min and max
    minmax = "{type}: ${price} - {date}"
    min = preds.loc[preds['price'].idxmin()]
    max = preds.loc[preds['price'].idxmax()]

    min_str = minmax.format(type="MIN", price=round(min.price, 2), date=min.date.split(" ")[0])
    max_str = minmax.format(type="MAX", price=round(max.price, 2), date=max.date.split(" ")[0])

    idraw.text((10,5), min_str + "   " + max_str, font=operator_date, fill='black')

    # %% Logo
    #idraw.text((center_x+170,height-50), "Almanac.Computer", fill='black', font=minotaur)

    issue_out = path = os.environ['TMA_home'] + '/_website/_current_issue/'
    img.save(issue_out + 'CCC_chart.gif', 'GIF')
