# Features (planets) chart
import numpy as np
import matplotlib
matplotlib.use('Agg')

import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import matplotlib.ticker as ticker

def chart_planets(future, planet_list, temp_output):
    # months for axis ticks - fill month_starts and drop columns
    future["month"] = future.index.strftime('%m').astype(int)
    future["month_diff"] = future.month.diff().fillna(0)
    month_starts = future.loc[future['month_diff'] != 0]
    future.drop(["month","month_diff"], axis=1,inplace=True)

    # remove backgrounds (has to be specified again for savefig())
    plt.rcParams['figure.facecolor'] = ((0,0,0,0.))
    plt.rcParams['axes.facecolor'] = ((0,0,0,0.))

    # create plot
    fig = plt.figure(figsize=(15,15), dpi=72)
    ax = plt.subplot(projection='polar')
    ax.set_theta_direction(-1)
    ax.set_theta_zero_location("N")

    # normalise theta axis and ticks
    t = mdates.date2num(future.index.to_pydatetime())
    tnorm = (t-t.min())/(t.max()-t.min())*2.*np.pi

    month_ticks= []
    month_labels = []

    for index, row in month_starts.iterrows():
        m = mdates.date2num(index.to_pydatetime())
        mnorm = (m-t.min())/(t.max()-t.min())*2.*np.pi
        month_ticks.append(mnorm)
        month_labels.append(index.strftime('%b %y'))

    ax.xaxis.set_major_locator(ticker.FixedLocator((month_ticks)))
    ax.xaxis.set_major_formatter(ticker.FixedFormatter((month_labels)))
    ax.xaxis.set_tick_params(pad=40, labelsize=25)

    # Y/Radial Axis - remove ticks
    ax.set_rticks([],[])
    # Remove outer circle
    ax.spines['polar'].set_visible(False)

    for planet in planet_list:
        col_name = planet + "_dist"
        ax.plot(tnorm, future[col_name],
            linewidth=2,
            linestyle="dotted",
            dashes=(1, 1),
            color = '#7786FF')

    plt.savefig(temp_output + "planets.png", facecolor = (0,0,0,0.), transparent=True)


def chart_preds(future, peaks_valleys, price_preds, temp_output):
    # create plot
    fig = plt.figure(figsize=(15,15), dpi=72)
    ax = plt.subplot(projection='polar')
    ax.set_theta_direction(-1)
    ax.set_theta_zero_location("N")

    # normalise theta axis and ticks
    t = mdates.date2num(future.index.to_pydatetime())
    tnorm = (t-t.min())/(t.max()-t.min())*2.*np.pi

    # set min and max
    import math
    price_min = future["pred_price"].min()
    axis_min = math.floor(price_min)

    price_max = future["pred_price"].max()
    axis_max = math.ceil(price_max)
    ax.set_ylim(axis_min, axis_max)

    peak_ticks= []
    peak_labels = []
    peak_values = []

    for index, row in peaks_valleys.iterrows():
        m = mdates.date2num(row['date'].to_pydatetime())
        mnorm = (m-t.min())/(t.max()-t.min())*2.*np.pi
        #peak_ticks.append(mnorm)
        peak_labels.append("${0:.2f} ".format(row['pred_price']))
        peak_values.append(row['pred_price'])
        if row['direction'] < 0:
            c = 'r'
        else:
            c = (0,1,0)
        ax.plot([axis_min,mnorm],[axis_min,axis_max], linestyle = '-', color = c)

    # theta ticks off
    ax.xaxis.set_major_locator(ticker.FixedLocator((peak_ticks)))

    # Radial ticks
    if peak_values:
        peak_values.sort()
        peak_labels.sort()
        peak_labels = [peak_labels[0], peak_labels[-1]]
        peak_values = [peak_values[0], peak_values[-1]]

    else:
        peak_values = [price_min, price_max]
        peak_labels = ["${0:.2f} ".format(x) for x in peak_values]

    ax.set_rgrids(peak_values, labels=None, color='k', angle=0, ha="left", zorder=10)
    ax.yaxis.set_tick_params(pad=25, labelsize=0, width=20)

    ax.grid(color="k")

    # plot block by block - green if start < end, red if start > end
    for name, block in price_preds.groupby(['block']):
        if block.head(1)["pred_price"].values < block.tail(1)["pred_price"].values:
            c = (0,1,0)
        else:
            c = 'r'
        # normalise theta axis and ticks
        year = mdates.date2num(price_preds.index.to_pydatetime())
        t = mdates.date2num(block.index.to_pydatetime())
        tnorm = (t-year.min())/(year.max()-year.min())*2.*np.pi

        ax.plot(tnorm, block["pred_price"],
            linewidth=5,
            color = c, zorder= 1)

    ax.spines['polar'].set_visible(False)
    plt.savefig(temp_output + "pred_price.png", facecolor = (0,0,0,0.), transparent=True)
