#!/bin/bash

source /home/pi/berryconda3/bin/activate /home/pi/berryconda3

cd /home/pi/TMA_checkout/

python Cover/cover_meta.py
python Cover/dial-gen-date.py
python Cover/dial-gen-earth.py
python Cover/dial-gen-stocks.py
python Cover/dial-gen-sun.py

python "Cosmic Commodity Chart/CCC_main.py"
python "Crisis Proximity Index/CPI_table.py"
# python "Carbon_Prophet/CP_main.py"
# Hashgets run at the end of the day in a different cron job
# python HASHGETS/hashget.py
python "r_astrologymemes/main.py"
python "Lotto/lotto_pred.py"
python "Electional Astrology/EA_main.py"
python "Power Grid Harmonics/PGH_main.py"

# archive new issue
time_stamp=$(date +%Y_%m_%d_%H_%M_%S)
cp -r /home/pi/TMA_checkout/_website/_current_issue/. /home/pi/TMA_checkout/_archive/$time_stamp

# commit issue
cd /home/pi/TMA_repo.git/
git add --a
time_stamp=$(date +%Y_%m_%d)
git commit --m "issue $time_stamp"

# push to Gitlab
HOME=/home/pi git push gitlab master

# copy to dat folder
cp -r /home/pi/TMA_checkout/_website/. /home/pi/TMA_dat

# publish dat
export NODE_PATH=/home/pi/.nvm/versions/node/v8.0.0/lib/node_modules
/home/pi/.nvm/versions/node/v8.0.0/bin/node /home/pi/TMA_checkout/dat_sharer.js
