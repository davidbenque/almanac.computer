# @Author: davidbenque
# @Date:   2018-05-03 1:44:46
# @Last modified by:   davidbenque
# @Last modified time: 2018-05-03 2:31:17

# %%
from datetime import date
from scipy.spatial import distance
import pandas as pd
import datetime
import os

# %%
print("CPI Starting: Crisis Proximity Index ", date.today())
# load skyfield and ephemeris
from skyfield.api import Loader, Topos

# Skyfield data
load = Loader(os.environ['TMA_home'] + '/_data/skyfield')
planets = load('de421.bsp')
ts = load.timescale()

# %%

earth = planets['earth']
planet_list = {
    'mercury': planets['mercury'],
    'venus': planets['venus'],
    'mars': planets['mars'],
    'jupiter': planets['jupiter barycenter']
    }

crisis_date = date(2007, 8, 9)

def crisis_pos(planet):
    astrometric = earth.at(ts.utc(crisis_date)).observe(planet_list[planet])
    crisis_pos = astrometric.position.au
    return crisis_pos

# %%


future = date.today() + datetime.timedelta(days=5)
time_diff = future - (crisis_date - datetime.timedelta(days=1))
time_range = time_diff.days
dates = []

for n in range(time_range):
    d = datetime.timedelta(days=n)
    dates.append(future - d)

# fill df with datetime index and mars positions
df = pd.DataFrame(data=dates, columns=['date'])
df = df.iloc[::-1]  # reverse

def planet_pos(x, planet):
    astrometric = earth.at(ts.utc(x)).observe(planet_list[planet])
    return astrometric.position.au

def dist_to_crisis(x, planet):
    d = distance.euclidean(crisis_pos(planet), x)
    return d


for p in planet_list:
    pos_colname = str(p) + '_pos'
    crisis_colname = str(p) + '_dist'
    df[pos_colname] = df['date'].apply(planet_pos, args = (p,))
    df[crisis_colname] = df[pos_colname].apply(dist_to_crisis, args = (p,))
    df.drop(pos_colname, axis = 1, inplace = True)

df['total'] = df.sum(axis=1)


# %%

df['mercury_diff'] = df['mercury_dist'].diff()
df['venus_diff'] = df['venus_dist'].diff()
df['mars_diff'] = df['mars_dist'].diff()
df['jupiter_diff'] = df['jupiter_dist'].diff()
df['total_diff'] = df['total'].diff()

df.fillna(value = 0, inplace = True)

def scale_diff(x, colmin, colmax):
    if x > 0.0:
        return x / colmax
    elif x < 0.0:
        return 0 - (x / colmin)
    else:
        return 0.0

diff_columns = ['mercury_diff', 'venus_diff', 'mars_diff', 'jupiter_diff', 'total_diff']

for colname in diff_columns:
    df[colname] = df[colname].apply(scale_diff, args=(df[colname].min(), df[colname].max(),))

today = date.today()
offset = datetime.timedelta(days=5)

start = today - offset
end = today + offset

crisis_matrix = df.loc[df['date'] >= start]

# %%
# export Table

output = os.environ['TMA_home'] + '/_website/_current_issue/CPI_table.csv'
crisis_matrix.to_csv(output, index=False)
print("Exported CPI table")
# %%
# positions

import json

dates = {'crisis': crisis_date, 'today':today}
GeoJSON = {'type': 'FeatureCollection', 'features': []}
paris = earth + Topos('48.864716 N', '2.349014 E')


diffs = crisis_matrix.loc[crisis_matrix['date'] == today]

def planet_latlon(planet, date):
    astrometric = paris.at(ts.utc(date)).observe(planet_list[planet])
    apparent = astrometric.apparent()
    ra, dec, dist = apparent.radec()
    lon = 360 * ra.hours / 24 - 180
    lat = dec.degrees
    return [lon, lat*2.5] #cheat to make lat more obvious

for planet in planet_list:
    planet_diff = diffs[planet + '_diff'].values[0]

    GeoJSON['features'].append({
        'type': 'Feature',
        'properties': {'planet': planet, 'date': 'today', 'diff': planet_diff},
        'geometry': {'type': 'LineString', 'coordinates': [planet_latlon(planet, crisis_date),planet_latlon(planet,today)]}
        })

    for date_name, date_value in dates.items():
        GeoJSON['features'].append({
            'type': 'Feature',
            'properties': {'date': date_name, 'planet': planet, 'diff': planet_diff},
            'geometry': {'type': 'Point', 'coordinates': planet_latlon(planet, date_value)}
            })


# %%
# Export GeoJSON file

output_geo = os.environ['TMA_home'] + '/_website/_current_issue/CPI_geo.json'

print("Exported CPI coordinates")
jsonfile = open(output_geo, 'w')
json.dump(GeoJSON, jsonfile)
jsonfile.close()
print("# END CPI #")
