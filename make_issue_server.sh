#!/bin/bash

source /home/almanac/.cache/pypoetry/virtualenvs/almanac.computer-KP59QZzb-py3.8/bin/activate /home/almanac/.cache/pypoetry/virtualenvs/almanac.computer-KP59QZzb-py3.8

cd /home/almanac/TMA_checkout/

python Cover/cover_meta.py
python Cover/dial-gen-date.py
python Cover/dial-gen-earth.py
python Cover/dial-gen-stocks.py
python Cover/dial-gen-sun.py

python "Cosmic Commodity Chart/CCC_main.py"
python "Crisis Proximity Index/CPI_table.py"
# python "Carbon_Prophet/CP_main.py"
# Hashgets run at the end of the day in a different cron job
# python HASHGETS/hashget.py
python "r_astrologymemes/main.py"
python "Lotto/lotto_pred.py"
python "Electional Astrology/EA_main.py"
python "Power Grid Harmonics/PGH_main.py"

# archive new issue
time_stamp=$(date +%Y_%m_%d_%H_%M_%S)
cp -r /home/almanac/TMA_checkout/_website/_current_issue/. /home/almanac/TMA_checkout/_archive/$time_stamp

# commit issue
cd /home/almanac/TMA_repo.git/
git add --a
time_stamp=$(date +%Y_%m_%d)
git commit --m "issue $time_stamp"

# push to Gitlab
HOME=/home/almanac git push gitlab master

