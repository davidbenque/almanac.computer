
# <codecell>

import datetime
import pandas as pd
import pandas_datareader.data as web
import os

# <codecell>

output = pd.DataFrame(columns=['name', 'value', 'min', 'max'])

symbols = ['GOOGL.US', 'AAPL.US', 'FB.US', 'AMZN.US']

for symbol in symbols:
    f = web.DataReader(symbol, 'stooq')
    df = f.head(8)['Close']
    df = df.iloc[::-1]
    df = df.pct_change()
    df = df.iloc[::-1]
    row = {
        'name': symbol[0],
        'value': df[0],
        'min':df.min(axis=0),
        'max': df.max(axis=0)}
    output = output.append(row, ignore_index=True)

# <codecell>

file_path = os.environ['TMA_home'] + '/_website/_current_issue/cover_dial_stocks.csv'
output.to_csv(file_path, index=False)

print("done stocks dial")
