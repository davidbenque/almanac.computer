# @Author: davidbenque
# @Date:   2018-04-30 5:10:32
# @Last modified by:   davidbenque
# @Last modified time: 2018-05-03 4:57:38

python dial-gen-date.py
python dial-gen-sun.py
python dial-gen-stocks.py
python dial-gen-earth.py
python cover_meta.py
