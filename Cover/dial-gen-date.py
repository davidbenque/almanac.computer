
# %%

from datetime import datetime
import csv
import os

output_path = os.environ['TMA_home'] + "/_website/_current_issue/cover_dial_date.csv"

now = datetime.now()

with open(output_path, "w") as csvfile:
    csvfile.write("name,value,min,max\n")
    csvfile.write("m,{},0,60\n".format(now.minute))
    csvfile.write("H,{},0,24\n".format(now.hour))
    csvfile.write("d,{},1,30\n".format(now.day))
    csvfile.write("M,{},1,12\n".format(now.month))
    csvfile.close()

print("done date dial")
