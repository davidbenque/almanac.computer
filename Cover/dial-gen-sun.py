
# %%

from datetime import date

from flatlib.datetime import Datetime
from flatlib.geopos import GeoPos
from flatlib.chart import Chart
from flatlib import const
from flatlib.aspects import aspectType
import csv
import os

# %%

output_path = os.environ['TMA_home'] + "/_website/_current_issue/cover_dial_sun.csv"


from flatlib import props
aspect_names = props.aspect.name

pos = GeoPos(51.424264, -0.072600) #London

now = date.today()
now_date = now.strftime('%Y/%m/%d')

chart = Chart(Datetime(now_date), pos)

sun = chart.get(const.SUN)

mercury = chart.get(const.MERCURY)
venus = chart.get(const.VENUS)
mars = chart.get(const.MARS)
moon = chart.get(const.MOON)

planets = {"☿": mercury, "♀":venus, "♂":mars, "☾":moon}

MIN = 0
MAX = 360

with open(output_path, 'w') as csv_file:
    colnames = ['name', 'value', 'min', 'max']
    writer = csv.DictWriter(csv_file, fieldnames = colnames)
    writer.writeheader()

    for symbol, p in planets.items():
        aspect = aspectType(sun, p, aspect_names)
        if aspect < 0:
            aspect = 0
        row = {"name": symbol, "value": aspect, "min": MIN, "max": MAX}
        writer.writerow(row)

print("done sun dial")
