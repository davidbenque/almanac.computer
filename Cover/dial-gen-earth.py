
# <codecell>
from datetime import date

import pandas as pd

from flatlib.datetime import Datetime
from flatlib.geopos import GeoPos
from flatlib.chart import Chart
from flatlib import const

import os

# %%

pos = GeoPos(51.424264, -0.072600) #London
now = date.today()
now_date = now.strftime('%Y/%m/%d')

chart = Chart(Datetime(now_date), pos)

# %%
# Add north node and south node

output = pd.DataFrame(columns=['name', 'value', 'min', 'max'])

NN_lon = chart.getObject(const.NORTH_NODE).lon
SN_lon = chart.getObject(const.SOUTH_NODE).lon

for k, i in {'☊':NN_lon, '☋':SN_lon}.items():
    row = {
        'name': k,
        'value': i,
        'min':0,
        'max': 360}
    output = output.append(row, ignore_index=True)

# %%
# Add Ascendant

ascendant = chart.getAngle(const.ASC)
row = {
    'name': 'A',
    'value': ascendant.lon,
    'min':0,
    'max': 360}
output = output.append(row, ignore_index=True)

# <codecell>
# Export

file_path = os.environ['TMA_home'] + '/_website/_current_issue/cover_dial_earth.csv'
output.to_csv(file_path, index=False)

print("done earth dial")
