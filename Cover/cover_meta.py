# @Author: davidbenque
# @Date:   2018-05-03 3:50:04
# @Last modified by:   davidbenque
# @Last modified time: 2018-05-03 4:50:37

from datetime import date
import os

print("## 📔 Starting Cover ## ")

today = date.today()
date_stamp = today.strftime('%Y-%m-%d')

JSON_out = {"date":date_stamp}

output = os.environ['TMA_home'] + '/_website/_current_issue/cover_meta.js'

with open(output, 'w') as file:
    file.write('var cover_meta_data = {};'.format(JSON_out))
    file.close()

print("done meta data")
