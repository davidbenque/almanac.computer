# almanac.computer  
aka *The Monistic Almanac*  

Almanacs are early prototypes of what we now know as “data analytics” and "algorithmic prediction." Published since at least the fifteenth century, almanacs are practical guides to the year ahead, comprising forecasts, calendars, and information in areas such as farming, nautical navigation, and finance. This project revisits the almanac as a site for critical making with data and code. It aims to unpack some of the history of prediction, and to question the divide between data science and divination.

A new issue of the almanac is published daily at http://almanac.computer

Further reading:  

[Project introduction](http://theairpump.davidbenque.com/anticipation-2017/)

Benque, D. (2018) ‘Cosmic Spreadsheets’, in Voss, G. (ed), *Supra Systems*, London, London College of Communication. [PDF [2.3Mb]](http://suprasystems.studio/downloads/book-chapters/Supra%20Systems%20Book_Chapter%2010_Benque.pdf)



## Installation

```
poetry install
```


---

This project is part of [David Benqué](http://davidbenque.com/)'s PhD research in Information Experience Design at the [Royal College of Art](https://www.rca.ac.uk/schools/school-of-communication/communication_research/) in London.  This work is supported by [Microsoft Research Cambridge](https://www.microsoft.com/en-us/research/lab/microsoft-research-cambridge/) (UK) through its PhD Scholarship Programme.
